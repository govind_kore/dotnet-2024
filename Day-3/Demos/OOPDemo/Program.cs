﻿using System;

namespace OOPDemo
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Person p1 = new Person();

            //p1.Set_Name("Govind");
            //string nameOfThePerson = p1.Get_Name();
            //Console.WriteLine(nameOfThePerson);
            //Console.ReadLine();

            //p1.Set_Name("1234");
            //string nameOfThePerson = p1.Get_Name();
            //Console.WriteLine(nameOfThePerson);
            //Console.ReadLine();

            //p1.Name = "Test";
            //string nameOfThePerson = p1.Name;

            //Console.WriteLine(nameOfThePerson);
            //Console.ReadLine();

            Console.WriteLine("Enter Your Name : ");
            p1.Name = Console.ReadLine();

            Console.WriteLine("Enter Your Mobile Number : ");
            p1.MobileNumber = Convert.ToInt64(Console.ReadLine());

            Console.WriteLine("Enter Your Address : ");
            p1.Address = Console.ReadLine();

            string details = p1.GetDetails();
            Console.WriteLine(details);

            Console.ReadLine();
        }
    }

    public class Person
    {
        private string _Name;
        private long _MoNo;
        private string _Address;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public long MobileNumber
        {
            get { return _MoNo; }
            set { _MoNo = value; }
        }

        public string Address
        {
            get { return _Address; }
            set { _Address = value; }
        }

        public string GetDetails()
        {
            return Name + " " + MobileNumber.ToString() + " " + Address;
        }

        //private string name;

        //public string Name
        //{
        //    get { return this.name; } 
        //    set
        //    {
        //        this.name = value;
        //    }
        //}

        //public void Set_Name(string name)
        //{
        //    if(name == "1234")
        //    {
        //        this.name = "Invalid Name !!!";
        //    }
        //    else
        //    {
        //        this.name = name;
        //    }
        //}

        //public string Get_Name() { return this.name; }
    }
}
