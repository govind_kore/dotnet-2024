﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoOOP00
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Employee p1 = new Employee();
            Console.WriteLine("Enter Your Name : ");
            p1.Name = Console.ReadLine();

            Console.WriteLine("Enter Your Number : ");
            p1.No = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter Your Address : ");
            p1.Address = Console.ReadLine();

            Console.WriteLine("You Are Working ? True/False : ");
            p1.IsWorking = Convert.ToBoolean(Console.ReadLine());

            Console.WriteLine("Enter Your Department : ");
            p1.Dept = Console.ReadLine();

            Console.WriteLine("You Entered Details : ");
            string details = p1.GetDetails();
            Console.WriteLine(details);

            Console.ReadLine();
        }
    }

    public class Person
    {
        private string _Name;
        private int _No;
        private string _Address;
        private bool _isWorking;

        public bool IsWorking
        {
            get { return _isWorking; }
            set { _isWorking = value; }
        }


        public string Address
        {
            get { return _Address; }
            set { _Address = value; }
        }


        public int No
        {
            get { return _No; }
            set { _No = value; }
        }

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public virtual string GetDetails() // If want to give permission to
        // its child class to give permisssion to override this method
        {
            return this.Name + ", " + this.Address + ", " 
                + this.No.ToString() + ", " + this.IsWorking;
        }
    }

    public class Employee:Person
    {
        private string _Dept;

        public string Dept
        {
            get { return _Dept; }
            set { _Dept = value; }
        }

        public override string GetDetails() // If we are overriding base calsse's method
        {
            return this.Name + ", " + this.Address + ", " 
                + this.No.ToString() + ", " + this.IsWorking + ", " + this.Dept;
        }
    }
}
