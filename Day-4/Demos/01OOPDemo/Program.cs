﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01OOPDemo
{
    internal class Program
    {
        static void Main(string[] args)
        {
            PersonSelector selector = new PersonSelector();

            Console.WriteLine("Whose details you would like to Enter : (1-Employee, 2-Customer) : ");
            int choice = Convert.ToInt32(Console.ReadLine());

            Person p1 = selector.GetSomePerson(choice);

            Console.WriteLine("You Entered Details : ");
            string details = p1.GetDetails();
            Console.WriteLine(details);

            Console.ReadLine();
        }
    }

    public class Person
    {
        private string _Name;
        private int _No;
        private string _Address;
        private bool _isWorking;

        public bool IsWorking
        {
            get { return _isWorking; }
            set { _isWorking = value; }
        }


        public string Address
        {
            get { return _Address; }
            set { _Address = value; }
        }


        public int No
        {
            get { return _No; }
            set { _No = value; }
        }

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public virtual string GetDetails() // If want to give permission to
        // its child class to give permisssion to override this method
        {
            return this.Name + ", " + this.Address + ", "
                + this.No.ToString() + ", " + this.IsWorking;
        }
    }

    public class Employee : Person
    {
        private string _Dept;

        public string Dept
        {
            get { return _Dept; }
            set { _Dept = value; }
        }

        //public override string GetDetails() // If we are overriding base calsse's method
        //{
        //    return this.Name + ", " + this.Address + ", "
        //        + this.No.ToString() + ", " + this.IsWorking + ", " + this.Dept;
        //}
        public override string GetDetails() // If we are overriding base calsse's method
        {
            return base.GetDetails() + ", " + this.Dept;
        }
    }

    public class Customer:Person
    {
        private string _OrderDetails;

        public string OrderDetails
        {
            get { return _OrderDetails; }
            set { _OrderDetails = value; }
        }

        //public override string GetDetails() // If we are overriding base calsse's method
        //{
        //    return this.Name + ", " + this.Address + ", "
        //        + this.No.ToString() + ", " + this.IsWorking + ", " + this.OrderDetails;
        //}
        public override string GetDetails() // If we are overriding base calsse's method
        {
            return base.GetDetails() + ", " + this.OrderDetails;
        }

    }

    public class PersonSelector                                     // Factory Class
    {
        public Person GetSomePerson(int choice)                     // Factory Method
        {
            if (choice == 1)
            {
                Employee e = new Employee();
                Console.WriteLine("Enter Your Name : ");
                e.Name = Console.ReadLine();

                Console.WriteLine("Enter Your Number : ");
                e.No = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Enter Your Address : ");
                e.Address = Console.ReadLine();

                Console.WriteLine("You Are Working ? True/False : ");
                e.IsWorking = Convert.ToBoolean(Console.ReadLine());

                Console.WriteLine("Enter Your Department : ");
                e.Dept = Console.ReadLine();

                return e;
            }
            else
            {
                Customer c = new Customer();
                Console.WriteLine("Enter Your Name : ");
                c.Name = Console.ReadLine();

                Console.WriteLine("Enter Your Number : ");
                c.No = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Enter Your Address : ");
                c.Address = Console.ReadLine();

                Console.WriteLine("You Are Working ? True/False : ");
                c.IsWorking = Convert.ToBoolean(Console.ReadLine());

                Console.WriteLine("Order Description : ");
                c.OrderDetails = Console.ReadLine();

                return c;
            }
        }
    }
}
