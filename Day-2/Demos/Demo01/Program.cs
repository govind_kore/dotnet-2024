﻿using System;
using MathLib;

namespace Demo01
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Maths obj = new Maths();

            while (true)
            {
                Console.WriteLine("Enter Value Of X : ");
                string data1 = Console.ReadLine();
                int i = Convert.ToInt32(data1);

                Console.WriteLine("Enter Value Of Y : ");
                string data2 = Console.ReadLine();
                int j = Convert.ToInt32(data2);

                Console.WriteLine("1: Add, 2: Sub, 3: Mult, 4:Div");
                Console.WriteLine("Enter Your Choice : ");
                int ch = Convert.ToInt32(Console.ReadLine());

                switch (ch)
                {
                    case 1:
                        int sum = obj.Add(i, j);
                        Console.WriteLine("Sum is : " + sum.ToString());
                        break;

                    case 2:
                        int sub = obj.Sub(i, j);
                        Console.WriteLine("Sub is : " + sub.ToString());
                        break;

                    case 3:
                        int mult = obj.Mult(i, j);
                        Console.WriteLine("Mult is : " + mult.ToString());
                        break;

                    case 4:
                        int div = obj.Div(i, j);
                        Console.WriteLine("Div is : " + div.ToString());
                        break;

                    default:
                        Console.WriteLine("Invalid Choice !!!");
                        break;
                }

                Console.WriteLine("Hitt Enter To Continue ....");
                Console.ReadLine();
            }
        }
    }
}
